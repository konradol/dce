<?php
namespace ArminVieweg\Dce\UserFunction\UserFields;

/*  | This extension is made for TYPO3 CMS and is licensed
 *  | under GNU General Public License.
 *  |
 *  | (c) 2012-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class InitCustomJavaScriptField
 *
 * @package ArminVieweg\Dce
 */
class InitCustomJavaScriptField
{
    /**
     * @param array $parameter
     * @return string
     */
    public function init(array $parameter)
    {
        /** @var \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance('TYPO3\CMS\Core\Page\PageRenderer');
        $extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath('dce');

        if (isset($_SERVER['SCRIPT_NAME'])) {
            $extPath = str_repeat('../', substr_count(dirname($_SERVER['SCRIPT_NAME']), '/')) . $extPath;
        }

        // Include JavaScripts
        $pageRenderer->addJsFile($extPath . 'Resources/Public/JavaScript/InitializeCodemirror.js');
        $pageRenderer->addJsFile($extPath . 'Resources/Public/JavaScript/EnhanceIrre.js');


        // Include Styles
        $pageRenderer->addCssFile($extPath . 'Resources/Public/JavaScript/Contrib/codemirror/lib/codemirror.css');
        $pageRenderer->addCssFile($extPath . 'Resources/Public/Css/custom_codemirror.css');

        return '';
    }
}
